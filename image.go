package nekos

import (
	"image"

	// Adds JPEG and PNG support
	_ "image/jpeg"
	_ "image/png"

	"net/http"

	"github.com/davecgh/go-spew/spew"
)

func getImage(url string) (*image.Image, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	img, _, err := image.Decode(resp.Body)
	if err != nil {
		spew.Dump(url)
		return nil, err
	}

	return &img, nil
}
