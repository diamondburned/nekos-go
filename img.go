package nekos

import (
	"encoding/json"
	"image"
	"io/ioutil"
	"net/http"
)

// ImageResponse contains the response for img
type ImageResponse struct {
	URL string `json:"url"`
}

// GetImg fetches a random image
func (imgtype ImageType) GetImg() (*image.Image, error) {
	resp, err := http.Get(Endpoint + "/img/" + string(imgtype))
	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	resp.Body.Close()

	imgresp := ImageResponse{}
	if err := json.Unmarshal(body, &imgresp); err != nil {
		return nil, err
	}

	return getImage(imgresp.URL)
}
